module gitlab.com/jpleau/ah/mysql

go 1.16

require (
	github.com/go-sql-driver/mysql v1.5.0
	gorm.io/driver/mysql v1.0.5
	gorm.io/gorm v1.21.6
)
