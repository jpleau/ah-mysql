package mysql

import (
	_ "github.com/go-sql-driver/mysql"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
	"gorm.io/gorm/migrator"
	"gorm.io/gorm/schema"
)

type Dialector struct {
	*mysql.Config
	Origin       mysql.Dialector
	DataCallback func(*Dialector, *schema.Field) string
}

func Open(dsn string, cb func(*Dialector, *schema.Field) string) gorm.Dialector {
	return &Dialector{
		Origin: mysql.Dialector{
			Config: &mysql.Config{DSN: dsn},
		},
		Config:       &mysql.Config{DSN: dsn},
		DataCallback: cb,
	}
}

func New(config mysql.Config, cb func(*Dialector, *schema.Field) string) gorm.Dialector {
	return &Dialector{
		Origin: mysql.Dialector{
			Config: &config,
		},
		Config:       &config,
		DataCallback: cb,
	}
}

func (d Dialector) Name() string {
	return "mysql"
}

func (d Dialector) Apply(config *gorm.Config) error {
	return d.Origin.Apply(config)
}

func (d Dialector) Initialize(db *gorm.DB) (err error) {
	return d.Origin.Initialize(db)
}

func (d Dialector) ClauseBuilders() map[string]clause.ClauseBuilder {
	return d.Origin.ClauseBuilders()
}

func (d Dialector) DefaultValueOf(field *schema.Field) clause.Expression {
	return d.Origin.DefaultValueOf(field)
}

func (d Dialector) Migrator(db *gorm.DB) gorm.Migrator {
	return Migrator{
		Migrator: migrator.Migrator{
			Config: migrator.Config{
				DB:        db,
				Dialector: d,
			},
		},
		Dialector: d,
	}
}

func (d Dialector) BindVarTo(writer clause.Writer, stmt *gorm.Statement, v interface{}) {
	d.Origin.BindVarTo(writer, stmt, v)
}

func (d Dialector) QuoteTo(writer clause.Writer, str string) {
	d.Origin.QuoteTo(writer, str)
}

func (d Dialector) Explain(sql string, vars ...interface{}) string {
	return d.Origin.Explain(sql, vars...)
}

func (d Dialector) DataTypeOf(field *schema.Field) string {
	if d.DataCallback == nil {
		return d.Origin.DataTypeOf(field)
	}

	return d.DataCallback(&d, field)
}

func (d Dialector) SavePoint(tx *gorm.DB, name string) error {
	return d.Origin.SavePoint(tx, name)
}

func (d Dialector) RollbackTo(tx *gorm.DB, name string) error {
	return d.Origin.RollbackTo(tx, name)
}
